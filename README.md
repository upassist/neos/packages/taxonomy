# README #

### What is this repository for? ###

* This package adds a taxonomy setup available to be used on your site.
* It adds:
    - default Tags (Document NodeType)
    - mixins to extend your NodeTypes to make them tagable
    - mixins to extend your NodeTypes to use them as a tag
    - a TagTree content nodetype based on the TYPO3.Neos.NodeTypes:Menu


### How do I get set up? ###

* Use this as a superType to make the NodeType a tag in itself: `'UpAssist.Neos.Taxonomy:Mixin.Tag'`
* Use this as a superType to make a NodeType 'tagable': `'UpAssist.Neos.Taxonomy:Mixin.Tagable'`

You can also make your listings 'tag-aware'. In order to do so make sure your Elastic Search query supports
the check for a property 'filter'.
You can add this property by extending your nodetype using the FilterableMixin: `'UpAssist.Neos.Taxonomy:FilterableMixin'`
