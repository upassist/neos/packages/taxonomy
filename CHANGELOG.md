# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [7.4.0](https://github.com/UpAssist/cnvinternationaal/compare/7.3.3...7.4.0) (2022-09-29)
